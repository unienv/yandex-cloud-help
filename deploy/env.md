# Переменные окружения

Переменные окружения (environment variables) указываются в системе и помогают настраивать приложени во время развертывания. В коде эту информацию можно получить из системных модулей. В переменных окружениях указываются, например, настройки подключения к внешним сервисам (базы данных, API-ключи...). 

## Указание переменных 

### На своем компьютере

- [C помощью команды export](https://ru.hexlet.io/blog/posts/bash-export)
- С помощью файла `.env` и модулей: [python](https://pypi.org/project/python-dotenv/), [nodejs](https://www.npmjs.com/package/dotenv) 

### В Gitlab

Указать переменные окружения можно в настройках проекта в Gitlab в разделе `Settings / CI/CD / Variables`.

![](../images/deploy/gitlab-env-vars.jpg)

::: warning Обратите внимание на параметр Protected
Если переменная помечена как protected, ветка тоже должна быть protected. Подробнее этот момент разобран [в видео](https://www.youtube.com/watch?v=MNzpcI33IT0&t=496s)
:::

## В Yandex Cloud Serverless Containers

Переменные указываются внутри вкладки "Редактор" на странице контейнера:

![](../images/deploy/serverless-env.png)

## Использование переменныж окружения

:::: tabs :options="{ useUrlFragment: false }"

::: tab "Python & Django"
**Получение значения из переменной окружения**

```python
variable = os.environ.get("ENV_VARIABLE", "default value")
```

**Фрагмент settings.py для Django**

Отключаем отладку по умолчанию. Если есть необходимость включить ее, нужно запустить сервер с указанием флага (`DEBUG=true python manage.py runserver`)

```python
DEBUG = os.environ.get("DEBUG", "false").lower() == "true"
```

Получаем настройки подключения к базе данных PostgreSQL из переменных окружения `DB_NAME`, `DB_USER`, `DB_PASSWORD`, `DB_HOST`

```python
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": os.environ.get("DB_NAME", "dbname"),
        "USER": os.environ.get("DB_USER", "dbuser"),
        "PASSWORD": os.environ.get("DB_PASSWORD", "dbpass"),
        "HOST": os.environ.get("DB_HOST", "dbhost"),
        "PORT": "5432",
    }
}
```
:::

::: tab NodeJS
Получение значения из переменной окружения:

```javascript
const variable = process.env.ENV_VARIABLE || 'default value';
```
:::

::::
