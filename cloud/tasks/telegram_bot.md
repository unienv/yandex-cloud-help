# Развертывание Telegram-бота

## Регистрация Telegram-бота
Для регистрации бота, перейдите в бота **@BotFather**. После выполнения необходимых действий для регистрации бота, вам будет выдан токен для доступа к **HTTP API**.


## Создание инфрастуктуры
Для работы бота необходимо создать Container Registry, Serverless Container и API Gateway. Подробнее об этом можно 
прочитать [тут](https://uenv.ru/help/cloud/serverless/)
Механизм работы:
1) Клиент отправляет POST-запрос на URL API Gateway
2) Serverless-контейнер обрабатывает запрос и возвращает ответ клиенту

## Работа с Webhook
Для того чтобы принимать запросы клиентов бота, пришедших на webhook и отдавать им ответы, необходимо добавить в код с ботом WSGI-приложение,
которое будет запускаться при старте контейнера. Пример запуска WSGI-приложения:
```python
def application(env, start_response):
    body = json.load(env['wsgi.input'])
    try:
        process_telegram_event(body)
        start_response('200 OK', [('Content-Type', 'text/html')])
    except Exception as e:
        print(e)
        start_response('400 Bad Request', [('Content-Type', 'text/html')])
```

[Репозиторий с примером кода](https://gitlab.com/unienv/test-projects/tg-bot-cloud)

## Регистрация Webhook
Для регистрации вебхука необходимо отправить GET запрос на следующий URL: https://api.telegram.org/bot{my_bot_token}/setWebhook?url={url_to_send_updates_to},
где *my_bot_token* — токен бота, а url_to_send_updates_to — url API Gateway

Команда для Gitlab CI:
```
curl https://api.telegram.org/bot{my_bot_token}/setWebhook?url={url_to_send_updates_to},
```