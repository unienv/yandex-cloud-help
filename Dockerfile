FROM node:16.15.0-alpine

# Установка зависимостей для awscli
RUN apk add --no-cache curl python3 py3-pip git && \
    pip install --upgrade awscli

WORKDIR /app

COPY package.json .
COPY package-lock.json .

RUN npm ci

COPY . .

ARG AWS_ACCESS_KEY_ID
ARG AWS_DEFAULT_REGION
ARG AWS_SECRET_ACCESS_KEY
ARG AWS_STORAGE_BUCKET_NAME
ARG YANDEX_METRIKA_ID
ENV YANDEX_METRIKA_ID $YANDEX_METRIKA_ID

ENV NODE_ENV production
RUN npm run build

RUN aws --endpoint-url=https://storage.yandexcloud.net s3 rm --recursive s3://$AWS_STORAGE_BUCKET_NAME
RUN aws --endpoint-url=https://storage.yandexcloud.net s3 cp --recursive static/ s3://$AWS_STORAGE_BUCKET_NAME/